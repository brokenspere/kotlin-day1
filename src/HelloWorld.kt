fun main(args: Array<String>) {
//   val x: Int =1
//    val y:Int =2
//    val z = x+y
//
//    var data : Int
//    var dataNulable: Int?
//    data = 2
//    println(y == data)
//    println(y === data)
//
//    dataNulable= null
//
//    println("Hello , world $z")

//    helloMore("Prayuth")
//    helloMore("Sanchai")
//
//    helloMore2("JunOcha","Prayuth")
//
//    gradeReport()
//    gradeReport("somchai",5.22)
//
//    gradeReport(name ="Nobita")
//    gradeReport(gpa = 2.5)
//
//    showName()
//
//    println(isOdd(5))
//
//    println(getAbbreviation('A'))
//
//    println(getGrade(75))
//
   //
    // println(getGradeResult(50))

//    for(i in 1..3){
//        println(i)
//    }

//    for(i in 6 downTo 0 step 2){
//        println(i)
//    }

//    var arrays = arrayOf(1,3,5,7)
//    for(i in arrays.indices){
//        println(arrays[i])
//    }


//    var arrays = arrayOf(1,3,5,7)
//    for ((index,value)in arrays.withIndex()){
//        println("$index. value = $value")
//    }

    var arrays = arrayOf(5,55,200,1,3,5,7)
    var max = findMaxValue(arrays)
    println("max value is $max")


}

fun helloMore(text: String): Unit {
    println("Hello $text")
}

fun helloMore2(surename: String ,name:String): Unit {
    println("Hello $surename , $name")
}


fun gradeReport (name: String="annoynymous",
                 gpa:Double = 0.00):Unit= println("mister $name gpa: is $gpa")

fun showName (name: String="allahu", surename: String="akbar"): Unit{
    println("Hello $name $surename")
}

fun isOdd (value:Int): String{
    if (value.rem(2)==0){

        return "$value is even value"
    }else{

        return "$value is odd value"
    }
}

fun getAbbreviation(abbr:Char):String{
    when (abbr){
        'A' -> {

            return "Abnormal"
        }
        'B' -> {
        return "Bad boy"
    }
        'F' -> {

        return "Fantastic"
    }
        'C' -> {
            println("not smart")
            return "cheap"
        }
        else-> return "Hello"
    }

}

fun getGrade (score: Int):String?{
    var grade:String?
    when(score){
        in 0..50-> grade="F"
        in 51..70-> grade="C"
        in 70..80-> grade="B"
        in 80..100-> grade="A"
        else -> grade=null
    }
    return grade
}
fun getGradeResult (score:Int):String?{
    val grade=getGrade(score)
    val result: String?

    when(grade){
        null -> result= "null ja"
        else -> result= "your score is $score "+getAbbreviation(grade.toCharArray()[0])
    }
    return result
}

fun findMaxValue(value: Array<Int>):Int{

    var max : Int = value[0]

    for (i in value){
        if(i >max){
            max =i
        }
    }

   return max

}
