


abstract class Person (var name: String ,var surname:String ,var gpa:Double){

    constructor(name: String , surname: String ):this(name,surname,0.0){

    }
    abstract fun goodBoy():Boolean
  open  fun getDetails():String{
        return "$name $surname has score $gpa"
    }
}

class Student (name: String,surname: String,gpa: Double,var department:String):Person(name,surname,gpa){

    override fun goodBoy(): Boolean {
        return gpa >2.0
    }

    override fun getDetails():String{
        return super.getDetails()+"and study in $department"
    }
}

data class Teacher (var name: String,var courseName:String ,var shritColor:Color)

enum class Color{
    RED,GREEN,BlUE
}

fun main (args:Array<String>){
//   val no1:Student = Student("bin","laden",2.00,"home")
////
////    val no2:Person = Person("prayuth","huador")
////    println(no2.getDetails())
////    val no3:Person = Person(surname = "prayu",name = "Son")
////    println(no3.getDetails())
//    println(no1.getDetails())

//    var teacher1:Teacher=Teacher("Somsak","Democratic",Color.GREEN)
//    var teacher2:Teacher=Teacher("Somsak","Democratic",Color.BlUE)
//    var teacher3:Teacher=Teacher("Somsak3","Democratic2",Color.RED)
//    val (teacherName , teacherCourseName) =teacher3
//
//    println(teacher1)
//    println(teacher2)
//    println(teacher3)

//    println(teacher1.equals(teacher3))
//    println(teacher1.equals(teacher2))
//
//    println(teacher1.courseName)
//    println(teacher1.name)
//
//    println(teacher1.component1())
//    println(teacher1.component2())

   // println("$teacherName teaches $teacherCourseName")

    var adHoc = object {
        var x: Int =0
        var y: Int = 1

    }
    println(adHoc)
    println(adHoc.x+ adHoc.y)
}